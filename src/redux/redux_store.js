import { combineReducers, createStore, applyMiddleware, compose } from "redux";
import thunkMiddleware from "redux-thunk";
import reducersAuth from "./reducersAuth";
import { reducer as formReducer } from "redux-form";
import reducerProduct from "./reducerProduct"
let reducers = combineReducers({
  reducerProduct:reducerProduct,
  reducersAuth: reducersAuth,
  form: formReducer,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
  reducers,
  composeEnhancers(applyMiddleware(thunkMiddleware))
);

// let store = createStore(reducers, applyMiddleware(thunkMiddleware));
window.store = store;
export default store;
