import { authAccount, getUser, outAccount } from "../firebase/firebase";
import firebase from "firebase/app";

const INITIALIZE_SUCCESS = `APP-INITIALIZE`;
let initialState = {
  initialize: false,
};

const reducersAuth = (state = initialState, action) => {
  switch (action.type) {
    case INITIALIZE_SUCCESS:
      return { ...state, initialize: action.initialize };
    default:
      return { ...state };
  }
};

export const setInitializeSuccessAC = (initialize) => {
  return {
    type: INITIALIZE_SUCCESS,
    initialize: initialize,
  };
};

export const getAuthenticationTC = () => {
  return async (dispatch) => {
    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        dispatch(setInitializeSuccessAC(true));
      } else {
        dispatch(setInitializeSuccessAC(false));
        return false;
      }
    });
  };
};

export const userLoginOutTC = () => {
  return async (dispatch) => {
    outAccount();
    dispatch(getAuthenticationTC());
  };
};

//test@test.test
//testtest

export const userLoginThunkCreator = (email, password) => {
  return async (dispatch) => {
    authAccount(email, password);
    getAuthenticationTC();
  };
};

export default reducersAuth;
