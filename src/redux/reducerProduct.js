import firebase from "firebase";
import {
  getTodos,
  createData,
  getData,
  loadImage,
  getDataProduct,
  changeProduct,
} from "./../firebase/firebase";
const GET_PRODUCT = "GET-PRODUCT";
const SET_INIT_VALUE = "SET-INIT-VALUE";
const FORM_SUBMIT = "FORM-SUBMIT";
let inistatate = {
  product: {},
  formSubmitData: "expectation",
};
const reducerProduct = (state = inistatate, action) => {
  switch (action.type) {
    case GET_PRODUCT:
      return { ...state, product: action.product };
    case SET_INIT_VALUE:
      let initialValues;
      if (state.product) {
        let productID = action.productID;
        initialValues = state.product[productID];
      } else {
        initialValues = {};
      }

      return { ...state, initialValues: initialValues };
    case FORM_SUBMIT:
      console.log(action.formSubmitData, `formSubmitData`);
      return { ...state, formSubmitData: action.formSubmitData };

    default:
      return state;
  }
};
export const setProductAC = (data) => {
  return { type: GET_PRODUCT, product: data };
};
export const setInitValueRedactProduct = (productID) => {
  return {
    type: SET_INIT_VALUE,
    productID: productID,
  };
};
export const setProductTC = (data) => {
  return (dispatch) => {};
};
export const getProductTC = () => {
  return async (dispatch) => {
    getDataProduct().then((productList) => {
      dispatch(setProductAC(productList));
    });
  };
};

const loadImag2 = (data, dispatch) => {
  var storageRef = firebase.storage().ref(`productImage/${data.name}`);

  var uploadTask = storageRef.put(data);
  uploadTask.on(
    firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
    function (snapshot) {
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;

      dispatch(formSubmittAC("Upload is " + progress + "% done"));
      // console.log("Upload is " + progress + "% done");
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED: // or 'paused'
          // console.log("Upload is paused");

          break;
        case firebase.storage.TaskState.RUNNING: // or 'running'
          // console.log("Upload is running");
          break;
        default:
          break;
      }
    }
  );
};

export const createProduct = (data) => {
  return (dispatch) => {
    loadImag2(data.image, dispatch);

    var copy = Object.assign({}, data);
    copy.image = `https://firebasestorage.googleapis.com/v0/b/newmarket-e1a76.appspot.com/o/productImage%2F${data.image.name}?alt=media`;
    let productID = copy.productName.replace(/\s/g, "");
    copy.productID = productID;

    createData(copy);
  };
};
export const formSubmittAC = (data) => {
  return {
    type: FORM_SUBMIT,
    formSubmitData: data,
  };
};

export const changeProductTC = (data) => {
  return (dispatch) => {
    loadImag2(data.image, dispatch);
    var copy = Object.assign({}, data);
    copy.image = `https://firebasestorage.googleapis.com/v0/b/newmarket-e1a76.appspot.com/o/productImage%2F${data.image.name}?alt=media`;
    changeProduct(copy);
  };
};
export default reducerProduct;
