import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withAuthRedirect } from "../Redirect/withAuthRedirect";
import {
  getProductTC,
  setInitValueRedactProduct,
  changeProductTC,
  formSubmittAC,
} from "../../redux/reducerProduct";
import { withRouter } from "react-router-dom";

import SimpleForm from "./SimpleFormRedact";
class ProductRedactContainer extends React.Component {
  componentDidMount() {
    this.props.getProductTC();
    let productId = this.props.match.params.productId;
    this.props.setInitValueRedactProduct(productId);
    this.props.formSubmittAC("expectation");
  }

  simpleFormSubmit(value) {
    let productId = this.match.params.productId;

    this.changeProductTC(value, productId);
  }

  render() {
    return (
      <div className="formWrapper-gl">
        <SimpleForm {...this.props} submitForm={this.simpleFormSubmit} />
        <div className="green">{this.props.formSubmitData}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    product: state.reducerProduct.product,
    formSubmitData: state.reducerProduct.formSubmitData,

    initialValues: state.reducerProduct.initialValues,
  };
};
export default compose(
  connect(mapStateToProps, {
    getProductTC,
    setInitValueRedactProduct,
    changeProductTC,
    formSubmittAC,
  }),
  withRouter,
  withAuthRedirect
)(ProductRedactContainer);
