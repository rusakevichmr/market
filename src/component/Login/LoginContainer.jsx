import React from "react";
import Login from "./Login";
import { connect } from "react-redux";
import { compose } from "redux";
// import { withRouter } from "react-router-dom";
import {
  userLoginThunkCreator,
  userLoginOutTC,
} from "../../redux/reducersAuth";

class LoginContainer extends React.Component {
  onSubmit = (data) => {
    this.props.userLoginThunkCreator(data.email, data.password);
  };
  offSubmit = () => {
    this.props.userLoginOutTC();
  };
  render() {
    return (
      <div>
        <Login
          {...this.props}
          onSubmit={this.onSubmit}
          offSubmit={this.offSubmit}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  initialize: state.reducersAuth.initialize,
});

export default compose(
  connect(mapStateToProps, {
    userLoginThunkCreator,
    userLoginOutTC,
  })
)(LoginContainer);
