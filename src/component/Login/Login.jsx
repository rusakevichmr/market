import React, { useEffect } from "react";
import s from "./Login.module.scss";
import { Field, reduxForm } from "redux-form";
import { required, email, length } from "redux-form-validators";
import { useHistory } from "react-router-dom";

const LoginForm = (props) => {
  const renderFieldEmail = (field) => (
    <div>
      <div>
        <input className={s.input} {...field.input} placeholder={"email"} />
        {!field.meta.valid ? <span className="red">email</span> : <span></span>}
      </div>
    </div>
  );

  const renderFieldPassword = (field) => (
    <div>
      <div>
        <input className={s.input} {...field.input} placeholder={"password"} />
        {!field.meta.valid ? (
          <span className="red">password</span>
        ) : (
          <span></span>
        )}
      </div>
    </div>
  );
  return (
    <div className={s.wrapperLogin}>
      <div className={s.formWrapperOuter}>
        <form className={s.form} onSubmit={props.handleSubmit}>
          <div className={s.Welcome}>Welcome to</div>
          <div className={s.GameStore}>GameStore</div>
          <div className={s.field}>
            <Field
              name="email"
              component={renderFieldEmail}
              type="email "
              className={s.input}
              validate={[required(), email(), length({ min: 5, max: 30 })]}
            />
          </div>
          <div className={s.field}>
            <Field
              placeholder={"password"}
              name="password"
              type={"password"}
              component={renderFieldPassword}
              className={s.input}
              validate={[required(), length({ min: 5, max: 30 })]}
            />
          </div>

          {props.error && (
            <div className={s.form_summary_error}>{props.error}</div>
          )}

          <div>
            <button className={s.button}>Login</button>
          </div>
        </form>
      </div>
      <div className={s.ingWrapperOuter}>
        <div className={s.img1}></div>
        <div className={s.img2}></div>
      </div>
    </div>
  );
};

const LoginReduxForm = reduxForm({
  form: `login`,
})(LoginForm);

const Login = (props) => {
  return (
    <div>
      <div>
        {props.initialize ? (
          <div className={s.wrapperLogin}>
            <div className={s.formWrapperOuter}>
              <button
                className={s.button}
                onClick={props.offSubmit}
                disabled={props.followingProgress}
              >
                LogOut
              </button>
            </div>
            <div className={s.ingWrapperOuter}>
              <div className={s.img1}></div>
              <div className={s.img2}></div>
            </div>
            <div></div>
          </div>
        ) : (
          <div>
            <LoginReduxForm onSubmit={props.onSubmit} />
          </div>
        )}
      </div>
    </div>
  );
};

export default Login;
