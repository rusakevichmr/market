import React from "react";
import { Field, Form } from "react-final-form";

const FormLogin = (props) => {
  return (
    <div>
      {!props.initialize ? (
        <Form
          onSubmit={(formObj) => {
            props.userLogin(formObj);
          }}
        >
          {({ handleSubmit }) => (
            <form onSubmit={handleSubmit}>
              <Field name="email">
                {({ input }) => (
                  <input placeholder="email" type="email" {...input} />
                )}
              </Field>
              <Field name="password">
                {({ input }) => (
                  <input placeholder="password" type="password" {...input} />
                )}
              </Field>
              <button type="submit">Submit</button>
            </form>
          )}
        </Form>
      ) : (
        <div>LogOut</div>
      )}
    </div>
  );
};
export default FormLogin;
