import React from "react";
import FormLogin from "./FormLogin";
import { connect } from "react-redux";
import { userLoginThunkCreator } from "../../redux/reducersAuth";
import { compose } from "redux";

class FormLoginContainer extends React.Component {

  userLogin(data) {
    this.userLoginThunkCreator(data.email, data.password);
  }
  render() {
    return (
      <div>
        <FormLogin {...this.props} userLogin={this.userLogin} />
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  initialize: state.reducersAuth.initialize,
});

export default compose(
  connect(mapStateToProps, {
    userLoginThunkCreator,
  })
)(FormLoginContainer);
