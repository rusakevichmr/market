import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withAuthRedirect } from "./../Redirect/withAuthRedirect";
import { createProduct, formSubmittAC } from "../../redux/reducerProduct";
import SimpleForm from "./SimpleForm/SimpleForm";
class ProductAddContainer extends React.Component {
  componentDidMount() {
    this.props.formSubmittAC(`expectation`);
  }
  simpleFormSubmit = (value) => {
    this.props.createProduct(value);
  };
  render() {
    return (
      <div className="formWrapper-gl">
        <SimpleForm
          productData={this.props}
          submitForm={this.simpleFormSubmit}
        />
        <div className="green">{this.props.formSubmitData}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    initialValues: state.reducerProduct.nameProduct,
    formSubmitData: state.reducerProduct.formSubmitData,
  };
};
export default compose(
  connect(mapStateToProps, { createProduct, formSubmittAC }),
  withAuthRedirect
)(ProductAddContainer);
