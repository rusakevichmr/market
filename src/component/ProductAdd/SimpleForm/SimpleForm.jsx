import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { Button, Form, Message, Card, Image, Grid } from "semantic-ui-react";
import { required, length, numericality } from "redux-form-validators";
import style from "../../../formStyle.module.scss";
class SimpleForm extends React.Component {
  static propTypes = {
    previewLogoUrl: PropTypes.string,
    mimeType: PropTypes.string,
    maxWeight: PropTypes.number,
    maxWidth: PropTypes.number,
    maxHeight: PropTypes.number,
    handleSubmit: PropTypes.func.isRequired,
  };
  static defaultProps = {
    previewLogoUrl:
      "https://firebasestorage.googleapis.com/v0/b/newmarket-e1a76.appspot.com/o/defimg.png?alt=media&token=93afb64a-edda-4a7d-b418-7542ae81972c",
    mimeType: "image/jpeg, image/png",
    maxWeight: 4000,
    maxWidth: 4000,
    maxHeight: 4000,
    errorName: "Error length min: 20, max: 60",
    errorPrise: "Error numericality",
    errorDiscount: "numericality 0 - 100 ",
  };

  validateImageWeight = (imageFile) => {
    if (imageFile && imageFile.size) {
      // Get image size in kilobytes
      const imageFileKb = imageFile.size / 1024;
      const { maxWeight } = this.props;

      if (imageFileKb > maxWeight) {
        return `Image size must be less or equal to ${maxWeight}kb`;
      }
    }
  };
  validateImageWidth = (imageFile) => {
    if (imageFile) {
      const { maxWidth } = this.props;

      if (imageFile.width > maxWidth) {
        return `Image width must be less or equal to ${maxWidth}px`;
      }
    }
  };
  validateImageHeight = (imageFile) => {
    if (imageFile) {
      const { maxHeight } = this.props;

      if (imageFile.height > maxHeight) {
        return `Image height must be less or equal to ${maxHeight}px`;
      }
    }
  };
  validateImageFormat = (imageFile) => {
    if (imageFile) {
      const { mimeType } = this.props;

      if (!mimeType.includes(imageFile.type)) {
        return `Image mime type must be ${mimeType}`;
      }
    }
  };
  handlePreview = (imageUrl) => {
    const previewImageDom = document.querySelector(".preview-image");
    previewImageDom.src = imageUrl;
  };
  handleChange = (event, input) => {
    event.preventDefault();
    let imageFile = event.target.files[0];
    if (imageFile) {
      const localImageUrl = URL.createObjectURL(imageFile);
      const imageObject = new window.Image();

      imageObject.onload = () => {
        imageFile.width = imageObject.naturalWidth;
        imageFile.height = imageObject.naturalHeight;
        input.onChange(imageFile);
        URL.revokeObjectURL(imageFile);
      };
      imageObject.src = localImageUrl;
      this.handlePreview(localImageUrl);
    }
  };
  renderFileInput = ({ input, type, meta }) => {
    const { mimeType } = this.props;
    return (
      <div>
        <input
          name={input.name}
          type={type}
          accept={mimeType}
          onChange={(event) => this.handleChange(event, input)}
        />
        {meta && meta.invalid && meta.error && (
          <Message negative header="Error:" content={meta.error} />
        )}
      </div>
    );
  };
  renderField = (field) => (
    <div>
      <label>{field.input.placeholder}</label>
      <div>
        <input
          className={style.input}
          placeholder={"productName"}
          {...field.input}
        />
        {!field.meta.valid ? (
          <span className="red">{this.props.errorName}</span>
        ) : (
          <span></span>
        )}
      </div>
    </div>
  );
  renderFieldPrise = (field) => (
    <div>
      <label>{field.input.placeholder}</label>
      <div>
        <input className={style.input} placeholder={"price"} {...field.input} />
        {!field.meta.valid ? (
          <span className="red">{this.props.errorPrise}</span>
        ) : (
          <span></span>
        )}
      </div>
    </div>
  );

  renderFieldDiscount = (field) => (
    <div>
      <label>{field.input.placeholder}</label>
      <div>
        <input
          className={style.input}
          placeholder={"discount"}
          {...field.input}
        />
        {!field.meta.valid ? (
          <span className="red">{this.props.errorDiscount}</span>
        ) : (
          <span></span>
        )}
      </div>
    </div>
  );

  handleSubmitForm = (values) => {
    this.props.submitForm(values);
  };

  render() {
    const {
      previewLogoUrl,
      maxWidth,
      maxHeight,
      maxWeight,
      handleSubmit,
    } = this.props;
    return (
      <Grid centered style={{ height: "100%" }} verticalAlign="middle" padded>
        <Grid.Column style={{ maxWidth: 400 }}>
          <Card fluid>
            <Image
              src={previewLogoUrl}
              alt="preview"
              className="preview-image"
              style={{ height: "300px", objectFit: "cover" }}
            />
            <Card.Content>
              <Form>
                <Form.Field>
                  <Field
                    name="image"
                    type="file"
                    validate={[
                      this.validateImageWeight,
                      this.validateImageWidth,
                      this.validateImageHeight,
                      this.validateImageFormat,
                    ]}
                    component={this.renderFileInput}
                    validate={[required()]}
                    className={style.input}
                  />
                </Form.Field>
                <Form.Field>
                  <Field
                    name="productName"
                    component={this.renderField}
                    type="text "
                    className={style.input}
                    validate={[required(), length({ min: 20, max: 60 })]}
                  />
                </Form.Field>
                <div>
                  <Field
                    placeholder={"productOptions"}
                    name="productOptions"
                    type={"text"}
                    component={"input"}
                    validate={[length({ max: 200 })]}
                    className={style.input}
                  />
                </div>
                <div>
                  <Field
                    name="price"
                    type={"text"}
                    component={this.renderFieldPrise}
                    validate={[
                      required(),
                      numericality({ ">": 1, "<=": 99999999 }),
                    ]}
                  />
                </div>
                <div>
                  <Field
                    name="discount"
                    type={"text"}
                    component={this.renderFieldDiscount}
                    validate={[numericality({ ">=": 0, "<=": 100 })]}
                  />
                </div>
                <div>
                  <Field
                    placeholder={"date"}
                    name="date"
                    type={"date"}
                    component={"input"}
                    className={style.input}
                  />
                </div>
                <Button
                  type="submit"
                  onClick={handleSubmit(this.handleSubmitForm)}
                  className={style.button}
                >
                  Submit
                </Button>
              </Form>
            </Card.Content>
          </Card>
        </Grid.Column>
      </Grid>
    );
  }
}

export default reduxForm({
  form: "simple",
})(SimpleForm);
