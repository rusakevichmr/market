import React from "react";
import style from "./ProductListStyle.module.scss";
import { NavLink } from "react-router-dom";
import { compose } from "redux";

const ProductList = (props) => {
  let product = props.productData;
  let productElement;

  const discountPrice = (price, discount) => {
    return price - (price / 100) * discount;
  };

  const discoundOff = (data) => {
    var msUTC = Date.parse(data); // зона UTC
    var today = new Date();
    let output = msUTC - today;
    output = Math.floor(output / 86400000);
    if (output >= 1) {
      return output;
    } else {
      return 0;
    }
  };

  if (product) {
    let producList = Object.keys(product).map((key) => {
      return product[key];
    });
    productElement = producList.map((cartProduct) => {
      // console.log(cartProduct);
      return (
        <div className={style.cartWrapperOuter} key={cartProduct.productName}>
          <div className={style.cartWrappeInner}>
            <img src={cartProduct.image} alt={cartProduct.productName} />
            <div className={style.textWrapper}>
              <div className={style.productName}>{cartProduct.productName}</div>
              <div>{cartProduct.productOptions}</div>
              {cartProduct.discount ? (
                <div>
                  {discountPrice(cartProduct.price, cartProduct.discount)}
                  <span className={style.spanDiscount}>Current Prise</span>
                </div>
              ) : (
                ``
              )}

              <div>
                {cartProduct.price} <span>Full Prise</span>{" "}
              </div>
              <div>
                {discoundOff(cartProduct.discount)}
                {cartProduct.discount}
                <span className={style.spanDiscount}>
                 end of the discount
                </span>
              </div>
              <div>
                {cartProduct.date}
                <span className={style.spanDiscount}>discount end date</span>
              </div>
              <div className={style.buttonWrapper}>
                <NavLink to={"/ProductRedact/" + cartProduct.productID}>
                  Edit product
                </NavLink>
              </div>
            </div>
          </div>
        </div>
      );
    });
  }

  return (
    <div>
      <div className={style.section_Wrapper}>{productElement}</div>
    </div>
  );
};

export default ProductList;
