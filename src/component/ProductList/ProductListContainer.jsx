import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { withAuthRedirect } from "../Redirect/withAuthRedirect";
import { getProductTC } from "../../redux/reducerProduct";
import ProductList from "./ProductList";
class ProductListContainer extends React.Component {
  componentDidMount() {
    this.props.getProductTC();
  }

  getProduct = (value) => {
    this.props.getProductTC();
  };

  discountPrice = (price, discount) => {
    return price - (price / 100) * discount;
  };
  currentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = [today.getMonth()];
    var yyyy = today.getFullYear();
    var day = [today.getDay()];
  }

  render() {
    return (
      <ProductList
        getProduct={this.getProduct}
        productData={this.props.product}
      />
    );
  }
}

let mapStateToProps = (state) => {
  return {
    product: state.reducerProduct.product,
  };
};
export default compose(
  connect(mapStateToProps, { getProductTC }),
  withAuthRedirect
)(ProductListContainer);
