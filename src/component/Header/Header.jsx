import React from "react";
import s from "./hederstyle.module.scss";
import { NavLink } from "react-router-dom";

const Header = (props) => {
  return (
    <header className={s.header_container}>
      <nav className={s.nav_container}>
        <div className={s.name}>GStore</div>
        <div>
          <NavLink to="/Login/"> User</NavLink>
        </div>
        <div>
          <NavLink to="/ProductList/">Product</NavLink>
        </div>
        <div>
          <NavLink to="/ProductAdd/">Product Add</NavLink>
        </div>
        <div>
          {/* <NavLink to="/ProductRedact/">Product Redact</NavLink> */}
        </div>
      </nav>
    </header>
  );
};

export default Header;
