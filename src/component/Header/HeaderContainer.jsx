import React from "react";
import Header from "./Header";
import { connect } from "react-redux";
import { compose } from "redux";

class HeaderContainer extends React.Component {
  render() {
    return <Header userData={this.props} />;
  }
}

let mapStateToProps = (state) => {
  return {
    // name: state.reducerApp.name,
  };
};
export default compose(connect(mapStateToProps, {}))(HeaderContainer);
