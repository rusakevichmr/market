import React from "react";
// import FormLoginContainer from "./FormLogin/FormLoginContainer";
import HeaderContainer from "./Header/HeaderContainer";
import { Route, Redirect } from "react-router-dom";
import ProductAddContainer from "./ProductAdd/ProductAddContainer";
import { connect } from "react-redux";
import { compose } from "redux";
import { getAuthenticationTC } from "../redux/reducersAuth";
import LoginContainer from "./Login/LoginContainer";
import ProductListContainer from "./ProductList/ProductListContainer";
import ProductRedactContainer from "./ProductRedact/ProductRedactContainer";

class App extends React.Component {
  componentDidMount() {
    this.props.getAuthenticationTC();
  }
  render() {
    return (
      <div>
        <HeaderContainer />
        <Route exact path="/" render={() => <LoginContainer />} />
        <Route path="/Login/" render={() => <LoginContainer />} />
        <Route path="/ProductAdd/" render={() => <ProductAddContainer />} />
        <Route path="/ProductList/" render={() => <ProductListContainer />} />
        <Route
          path="/ProductRedact/:productId?"
          render={() => <ProductRedactContainer />}
        />
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return { initialize: state.reducersAuth.initialize };
};

export default compose(connect(mapStateToProps, { getAuthenticationTC }))(App);
