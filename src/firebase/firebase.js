import firebase from "firebase";

firebase.initializeApp({
  apiKey: "AIzaSyDmSVR_jOwYCn2EwYllsrdTV9A056Gz0n4",
  authDomain: "newmarket-e1a76.firebaseapp.com",
  databaseURL: "https://newmarket-e1a76.firebaseio.com",
  projectId: "newmarket-e1a76",
  storageBucket: "newmarket-e1a76.appspot.com",
  messagingSenderId: "35022491716",
  appId: "1:35022491716:web:d22f031cce8f2f2805ac11",
});
const db = firebase.firestore();
const auth = firebase.auth();
const fireDat = firebase.database();

const createAccount = (email, password) => {
  return auth
    .createUserWithEmailAndPassword(email, password)
    .then((data) => console.log(data, "them"))
    .catch((data) => console.log(data, "error"));
};

const authAccount = (email, password) => {
  return auth
    .signInWithEmailAndPassword(email, password)
    .then((data) => console.log(data, "then"))
    .catch((data) => console.log(data, "catch"));
};

const outAccount = () => {
  return auth.signOut();
};

const getUser = () => {
  return auth.onAuthStateChanged(function (user) {
    if (user) {
      return true;
    } else {
      return false;
    }
  });
};

const loadImage = (data) => {
  var storageRef = firebase.storage().ref(`productImage/${data.name}`);

  var uploadTask = storageRef.put(data);
  uploadTask.on(
    firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
    function (snapshot) {
      var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log("Upload is " + progress + "% done");
      switch (snapshot.state) {
        case firebase.storage.TaskState.PAUSED: // or 'paused'
          console.log("Upload is paused");

          break;
        case firebase.storage.TaskState.RUNNING: // or 'running'
          console.log("Upload is running");
          break;
        default:
          break;
      }
    }
  );
};

const createData = (data) => {
  return firebase
    .database()
    .ref("product/" + data.productID)
    .set({
      ...data,
    });
};

const changeProduct = (data) => {
  return firebase
    .database()
    .ref("product/" + data.productID)
    .set({
      ...data,
    });
};

const getDataProduct = () => {
  // var userId = firebase.auth().currentUser.uid;
  return firebase
    .database()
    .ref("/product/")
    .once("value")
    .then((snapshot) => snapshot.val());
};

export {
  db,
  createAccount,
  authAccount,
  getUser,
  outAccount,
  createData,
  loadImage,
  getDataProduct,
  changeProduct,
  fireDat,
};
